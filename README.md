# README

target は 0, 1

2時間

## data
- train 1.3M, insincreは6.8%だけ (imbalanced -> f1 score)
test.csv 1stageでは56k, 2stageで376k
多くて80ワードくらい


各種embeddings
- google news vector: word2vec, 300次元、3M words
- glove : glove, 300d, 2.2M words, cased
- paragram :  Paraphrase Database からなんか学習したっぽい: 
- wiki-news : faltText 1M words

embeddingsの論文読もう

どうやって使うのか

論文の保存のしかた、検索

## Kernels

昔のコンペの読む
https://www.kaggle.com/c/quora-insincere-questions-classification/discussion/70788#latest-424668

embedinngsいっぱいあるのどう使うか
https://www.kaggle.com/c/quora-insincere-questions-classification/discussion/71778#latest-436128

texst の augmentation
https://www.kaggle.com/c/quora-insincere-questions-classification/discussion/71083

blending
https://www.kaggle.com/c/quora-insincere-questions-classification/discussion/72627

Helpful material
https://www.kaggle.com/c/quora-insincere-questions-classification/discussion/71361
このなかにfast.aiのビデオあった

pytorchやっぱ評判いいわ
でもkerasも読めないと辛い

deterministic resutls
https://www.kaggle.com/c/quora-insincere-questions-classification/discussion/72040

https://www.kaggle.com/c/quora-insincere-questions-classification/discussion/73341

f1 score optimization
https://www.kaggle.com/c/quora-insincere-questions-classification/discussion/71184

misslabeled samples
https://www.kaggle.com/c/quora-insincere-questions-classification/discussion/71429
https://www.kaggle.com/ratthachat/handle-overfitting-error-analysis-of-glove-gru
https://www.kaggle.com/c/quora-insincere-questions-classification/discussion/70956

前処理
https://www.kaggle.com/theoviel/improve-your-score-with-text-preprocessing-v2

### LB

cnn, attention, gru, lstmの5モデルをblendしてる
https://www.kaggle.com/ashishpatel26/nlp-text-analytics-solution-quora

これプラス
- 前処理
- misslabeled samples
    - 長さを特徴にするとか
    - 閾値できる
    - 宗教的なワードはinsincereにする
- imbalanced samples

- text augmentation
- 
- f1 score, clr
https://www.kaggle.com/shujian/single-rnn-with-4-folds-clr

- capsule
https://www.kaggle.com/gmhost/gru-capsule

- auxiliary task



## models
GRU
CRF
LSTM
Conv1d
Attension
- masking, branch


## idea
- 短いquestionはinsincreにする

- imbalance性はどうするの？

## libraries
- tqdm: 時間図れる
- textstat: readabilityとかいろいろとれる
- nltk: 自然言語処理 ngram, stopwords
- spacy
- from keras.preprocessing.text import Tokenizer





